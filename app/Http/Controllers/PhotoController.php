<?php

namespace App\Http\Controllers;

use App\Photo;
use App\User;
use App\UserPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GeoIP as GeoIP;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    public function show_send()
    {
        $uri = "photos_send";
        return view('photos.send', compact('uri'));
    }

    public function show_receive()
    {
        $uri = "photos_receive";

        $location = GeoIP::getLocation();
        $lat = $location['lat'];
        $lng = $location['lon'];

        $user = User::find(Auth::id());

        if ($user == null)
        {
            $photosReceived = null;
        }
        else
        {
            $photosReceived = UserPhoto::where('user_to_id', $user->id)->get();
        }


        $photosInYourArea = PhotoController::findNearestPhotos($lat, $lng);

        return view('photos.receive', compact('uri','photosInYourArea', 'photosReceived'));  
    }

    public function findNearestPhotos($latitude, $longitude, $radius = 100)
    {
        /*
         * replace 6371000 with 6371 for kilometer and 3956 for miles
         */
        $photos = Photo::selectRaw("id, image_path, lat, lng,
                         ( 6371000 * acos( cos( radians(?) ) *
                           cos( radians( lat ) )
                           * cos( radians( lng ) - radians(?)
                           ) + sin( radians(?) ) *
                           sin( radians( lat ) ) )
                         ) AS distance", [$latitude, $longitude, $latitude])
            ->having("distance", "<", $radius)
            ->orderBy("distance",'asc')
            ->offset(0)
            ->limit(20)
            ->get();

        return $photos;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        //
    }
}
