<?php

namespace App\Http\Controllers;

use App\UserPhotoComment;
use Illuminate\Http\Request;

class UserPhotoCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserPhotoComment  $userPhotoComment
     * @return \Illuminate\Http\Response
     */
    public function show(UserPhotoComment $userPhotoComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserPhotoComment  $userPhotoComment
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPhotoComment $userPhotoComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserPhotoComment  $userPhotoComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPhotoComment $userPhotoComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserPhotoComment  $userPhotoComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPhotoComment $userPhotoComment)
    {
        //
    }
}
