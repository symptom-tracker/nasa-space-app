<?php

namespace App\Http\Controllers;

use App\UserPhotoLike;
use Illuminate\Http\Request;

class UserPhotoLikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserPhotoLike  $userPhotoLike
     * @return \Illuminate\Http\Response
     */
    public function show(UserPhotoLike $userPhotoLike)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserPhotoLike  $userPhotoLike
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPhotoLike $userPhotoLike)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserPhotoLike  $userPhotoLike
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPhotoLike $userPhotoLike)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserPhotoLike  $userPhotoLike
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPhotoLike $userPhotoLike)
    {
        //
    }
}
