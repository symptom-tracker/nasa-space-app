<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPhoto extends Model
{
    
  use SoftDeletes;

  public static function boot()
  {
    parent::boot();

    static::deleting(function ($userPhoto) {
      foreach ($userPhoto->comments as $comment) {
        $comment->delete();
      }
      
      foreach ($userPhoto->likes as $like) {
        $like->delete();
      }
    });
  }

  public function photoReceives()
  {
    return $this->belongsTo('App\User', 'user_from_id', 'id');
  }

  public function photoSends()
  {
    return $this->belongsTo('App\User', 'user_to_id', 'id');
  }

  public function photos()
  {
    return $this->hasMany('App\Photo', 'photo_id', 'id');
  }
}
