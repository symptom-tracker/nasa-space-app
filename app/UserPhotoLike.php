<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPhotoLike extends Model
{
    use SoftDeletes;

    public function photo() {
        return $this->belongsTo('App\UserPhoto', 'photo_id', 'id');
    }
}
