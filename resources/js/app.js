// import './bootstrap';
import './modal-functions';

$(document).ready(function () {

  // on scroll
  $(window).scroll(function () {
    if ($(window).scrollTop() > 300) {
      $('.scroll-top').addClass('show');
    }
    else {
      $('.scroll-top').removeClass('show');
    }
  });
  $(".scroll-top").on('click', function (event) {
    $('html, body').animate({ scrollTop: 0 }, '500');
  });

  $(document).click(function (event) {
    $(".navbar-collapse").collapse('hide');
  });

});
