{{-- status alerts --}}
@if(session('status'))
  <div class="alert alert-success alert-dismissible fade show">
    {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
{{-- primary alerts --}}
@if(session('primary'))
  <div class="alert alert-primary alert-dismissible fade show">
    {{ session('primary') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
{{-- success alerts --}}
@if(session('success'))
  <div class="alert alert-success alert-dismissible fade show">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
{{-- warning alerts --}}
@if(session('warning'))
  <div class="alert alert-warning alert-dismissible fade show">
    {{ session('warning') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
{{-- danger alerts --}}
@if(session('danger'))
  <div class="alert alert-danger alert-dismissible fade show">
    {{ session('danger') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
