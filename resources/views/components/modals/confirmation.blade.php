<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content p-2">

      <div class="modal-body">

        <div class="d-flex flex-column justify-content-center align-items-center">
          <h5 class="modal-title">
            Are you sure?
          </h5>
          <div class="mt-4">
            <form method="POST">
              @method('PUT')
              @csrf
              <button type="button" class="btn btn-link text-decoration-none" data-dismiss="modal">Cancel</button>
              <button-submit text="Yes" class="btn-primary"/>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
