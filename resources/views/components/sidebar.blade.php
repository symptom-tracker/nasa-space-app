<card-basic>
  {{-- TO-DO: Add icons to links --}}
  <ul class="list-group">
    <a href="{{ route('profile') }}" class="list-group-item list-group-item-action border-0 rounded-0 text-uppercase
      {{ isset($uri) && $uri == 'profile' ? 'active' : '' }}">
    	Profile
    </a>
    <a href="{{ route('photos.receive') }}" class="list-group-item list-group-item-action border-0 rounded-0 text-uppercase
      {{ isset($uri) && $uri == 'photos_receive' ? 'active' : '' }}">
    	View Photos
    </a>
    <a href="{{ route('photos.send') }}" class="list-group-item list-group-item-action border-0 rounded-0 text-uppercase
      {{ isset($uri) && $uri == 'photos_send' ? 'active' : '' }}">
    	Send Photos
    </a>
    <a href="{{ route('connections') }}" class="list-group-item list-group-item-action border-0 rounded-0 text-uppercase
      {{ isset($uri) && $uri == 'connections' ? 'active' : '' }}">
    	Connections
    </a>
    <div class="dropdown-divider"></div>
    <a href="#" class="list-group-item list-group-item-action border-0 rounded-0 text-uppercase">Logout</a>
  </ul>
</card-basic>
