<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'NASA Space Apps Challenge') }}</title>

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Open Graph meta data tags --}}
    <meta property="og:url" content="{{ config('app.url') }}" />

    {{-- Icon --}}
    {{-- <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"> --}}
    {{-- <link rel="icon" href="{{ asset('images/favicon.png') }}"> --}}

    {{-- Bootstrap core CSS --}}
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    {{-- AOS --}}
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    {{-- Fonts --}}
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

    @yield('styles')

  </head>
  <body class="bg-dark">

    <div id="app">

      {{-- Scroll-To-Top Button --}}
      <div class="se-pre-con"></div>
      <a id="scroll-top-button" class="scroll-top">
        <i class="material-icons text-white" style="margin-top: 12px;">keyboard_arrow_up</i>
      </a>

      {{-- Modals --}}
      @include('components.modals.confirmation')
      @include('components.modals.delete-confirmation')

      <div id="header" class="container d-flex justify-content-center">
        <h1 class="display-3 text-white align-self-center">Nasa Space Apps Challenge</h1>
      </div>

      {{-- Content --}}
      @yield('content')

      {{-- Footer --}}
      <div class="d-flex justify-content-center py-4 text-muted">
        footer, i am
      </div>

    </div>

    {{-- Bootstrap --}}
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    {{-- Laravel Mix --}}
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/vue.js') }}" defer></script>

    @yield('scripts')

  </body>
</html>
