@extends('layouts.app.index')

@section('content')
  <div class="container p-3 p-lg-5">
    <div class="row">
      {{-- Sidebar --}}
      <div class="col-lg-3">
        @include('components.sidebar')
      </div>

      {{-- Main Content --}}
      <div class="col-lg-9">
        @if(isset($photosReceived))
          <div class="row mb-4">
            <div class="col">
              <card-basic title="Receive Photos">
                @foreach($photosReceived as $photo)
                  <img src="{{ Storage::disk('public')->url($photo->image_path) }}" class="img-thumbnail" height="32" width="32">
                @endforeach
              </card-basic>
            </div>
          </div>
        @endif
        <div class="row mb-4">
          <div class="col">
            <card-basic title="Photos in my area">
              @foreach($photosInYourArea as $photo)
                <img src="{{ Storage::disk('public')->url($photo->image_path) }}" class="img-thumbnail" height="32" width="32">
              @endforeach
            </card-basic>
          </div>
        </div>
      </div>
    </div>

    

  </div>
@endsection
