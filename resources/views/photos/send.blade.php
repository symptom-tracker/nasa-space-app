@extends('layouts.app.index')

@section('content')
  <div class="container p-3 p-lg-5">

    <div class="row">
      {{-- Sidebar --}}
      <div class="col-lg-3">
        @include('components.sidebar')
      </div>
      {{-- Main Content --}}
      <div class="col-lg-9">
        <card-basic title="Send Photos">
          card
        </card-basic>
      </div>
    </div>

  </div>
@endsection
