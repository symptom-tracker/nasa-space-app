@extends('layouts.app.index')

@section('content')
  <div class="container p-3 p-lg-5">

    <div class="row">
      {{-- Sidebar --}}
      <div class="col-lg-3">
        @include('components.sidebar')
      </div>
      {{-- Main Content --}}
      <div class="col-lg-9">
        <card-basic title="My Profile">
          {{-- Edit and Delete options --}}
          <template v-slot:options>
            <options-dropdown>

              {{-- Confirm --}}
              <a href="{{ route('profile.edit', ['id' => 1])}}" class="dropdown-item text-primary">
                Edit
              </a>

              {{-- Delete --}}
              <button class="dropdown-item text-danger" type="button"
                data-toggle="modal"
                data-target="#deleteConfirmationModal"
                data-title="Delete Announcement"
                data-action="#">
                Delete
              </button>

            </options-dropdown>
          </template>

          <div class="d-flex align-content-between">
            {{-- Details --}}
            <div class="flex-grow-1">
              {{-- Name --}}
              <div class="form-group">
                <label for="name" class="text-muted">Name</label>
                <p class="lead" id="name">John Doe</p>
              </div>
              {{-- Email --}}
              <div class="form-group">
                <label for="email" class="text-muted">Email Address</label>
                <p class="lead" id="email">john_doe@nasa.app.com</p>
              </div>
            </div>
            {{-- Edit Btn --}}
            <div class="text-right">
              {{-- TO-DO: Replace ID with Auth::id() --}}
              <a href="{{ route('profile.edit', ['id' => 1])}}" class="btn btn-light btn-sm" type="button">
                <i class="material-icons">edit</i>
              </a>
            </div>
          </div>
        </card-basic>

        <card-basic title="Photos I've Sent" class="mt-4">
          <div class="jumbotron rounded-0">
            {{-- Images --}}
          </div>
          <template>
            <a href="{{ route('photos.send') }}" class="btn btn-primary w-100 py-2 text-uppercase">Send Photos</a>
          </template>
        </card-basic>
      </div>
    </div>

  </div>
@endsection
