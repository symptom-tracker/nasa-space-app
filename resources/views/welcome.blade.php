@extends('layouts.app.index')

@section('styles')
@endsection

@section('content')
  <div class="container">

    <h1 id="top" class="mt-4">
      Hello, World!
    </h1>

    <h5 class="mt-4">
      <link-scroll href="#hi">
        Click me to go down! (make sure the screen is small)
      </link-scroll>
    </h5>

    <card-basic title="Hi!" class="mt-4">
      <template v-slot:options>
        <options-dropdown>

          {{-- Confirm --}}
          <button class="dropdown-item" type="button"
            data-toggle="modal"
            data-target="#confirmationModal"
            data-title="Confirm"
            data-action="#">
            Confirm
          </button>

          {{-- Delete --}}
          <button class="dropdown-item text-danger" type="button"
            data-toggle="modal"
            data-target="#deleteConfirmationModal"
            data-title="Delete Announcement"
            data-action="#">
            Delete
          </button>

        </options-dropdown>
      </template>
      Hello
    </card-basic>

    <card-basic class="mt-4">
      <p class="text-justify">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sed auctor sem. Vestibulum a pharetra diam. Donec erat odio, semper vitae sapien eu, elementum rutrum augue. Cras ultricies, risus sed cursus gravida, lectus mi viverra tellus, eget mollis ex tortor id erat. Suspendisse potenti. Morbi porta velit efficitur eros tincidunt dapibus. Duis placerat efficitur posuere. Sed odio ipsum, fermentum in tincidunt elementum, maximus ut massa.
      </p>
      <p class="text-justify">
        Nunc sit amet scelerisque quam, quis bibendum ante. Nulla tempus nisi eget eros interdum ullamcorper. Donec varius ligula a dolor venenatis cursus. Nunc ac volutpat nulla. Praesent tristique urna laoreet ante tincidunt bibendum. Curabitur scelerisque libero nec tincidunt ornare. Etiam malesuada justo vitae tellus molestie imperdiet. Vestibulum nibh nisl, maximus sed metus id, tincidunt maximus erat. Morbi egestas tincidunt nisi, ut pulvinar lorem varius vel. Suspendisse suscipit eleifend efficitur. Praesent porta dolor lectus, ac venenatis nunc pellentesque ac. Proin posuere purus diam, in placerat mi scelerisque eget. Proin vel metus vitae neque mattis pulvinar. Cras ac eros ut nibh imperdiet sollicitudin. Aenean vestibulum libero a ex laoreet volutpat.
      </p>
      <p class="text-justify">
        Vivamus vel consectetur enim. Maecenas elementum leo turpis, sed interdum tortor facilisis vitae. Donec at nulla et odio fermentum venenatis non in metus. Phasellus tempor ex id mi varius gravida. Morbi a tortor ipsum. Phasellus ipsum dui, dignissim efficitur cursus sit amet, pulvinar non quam. Integer libero erat, rutrum id suscipit eu, pulvinar vitae urna. Donec hendrerit ac metus nec tincidunt. Nunc sed feugiat nibh. Nam bibendum ligula a interdum porta. In laoreet diam in risus lobortis pharetra sed in mi. Nulla rhoncus nunc tortor, blandit vehicula felis condimentum vel.
      </p>
      <p class="text-justify">
        Suspendisse potenti. Donec tincidunt eu mauris quis varius. Donec ac sodales neque, sed efficitur odio. Integer vitae laoreet sem. Curabitur urna nunc, maximus eget rhoncus a, rutrum sit amet nulla. Donec eget lacus vel nulla congue eleifend at at nunc. Donec placerat vitae enim ac suscipit. Donec eu aliquet leo.
      </p>
      <p class="text-justify">
        Aenean lacus elit, vehicula eu tempor varius, blandit vitae orci. Sed vitae enim faucibus mauris ullamcorper condimentum. Donec vestibulum urna et quam porta suscipit. Praesent venenatis dictum neque, ut mollis mauris fermentum non. Mauris tincidunt enim vel arcu interdum facilisis. Vivamus non enim in risus cursus facilisis. Maecenas dapibus libero maximus aliquet commodo. Phasellus sit amet blandit tellus, vel cursus erat.
      </p>
    </card-basic>

    <card-basic id="hi" title="Hi" class="mt-4">
      <link-scroll href="#top">
        go up
      </link-scroll>
    </card-basic>

    <button-submit class="mt-4"/>

  </div>
@endsection

@section('scripts')
@endsection
