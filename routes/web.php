<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TO-DO: Add 'auth' middleware for group below:
Route::middleware([])->group(function() {
	Route::get('profile', 'UserController@profile')->name('profile');
	Route::get('profile/{id}/edit', 'UserController@edit')->name('profile.edit');

	Route::prefix('photos')->name('photos.')->group(function() {
		Route::get('send', 'PhotoController@show_send')->name('send');
		Route::get('receive', 'PhotoController@show_receive')->name('receive');
	});

	Route::get('connections', 'UserController@connections')->name('connections');
});

Route::get('/', function () {
    // return view('welcome');
    return redirect(route('profile'));
});
